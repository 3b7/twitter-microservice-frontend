class LandingController < ApplicationController
  before_filter :authenticate, :if => lambda { Rails.env.production? }
  
  def index
  end
end